//go:generate goversioninfo -icon=dokuco.ico
package main

import (
	"context"
	"fmt"
	"github.com/common-nighthawk/go-figure"
	"github.com/digitalocean/godo"
	"github.com/fatih/color"
	"golang.org/x/oauth2"
	"io/ioutil"
	"os/user"
	"path"
)

var homeDir string

func main() {

	figure.NewFigure("dokuco", "doom", false).Print()

	fmt.Print("\n\n")

	user, err := user.Current()
	if err != nil {
		panic(fmt.Sprintf("could not read current user: %v", err))
	}

	homeDir = user.HomeDir
	doConfFile := path.Join(homeDir, ".kube/do.conf")

	tokenData, err := ioutil.ReadFile(doConfFile)
	if err != nil {
		panic(fmt.Sprintf("could not read config filefrom ~/do.conf: %v", err))
	}

	token := string(tokenData)
	clusters := listClusters(token)
	for _, cluster := range clusters {
		color.Yellow("+++ Fetch kube config for cluster %s", cluster.Name)
		fetchConfig(token, cluster)
	}

}

func fetchConfig(token string, cluster *godo.KubernetesCluster) string {

	tokenSource := &TokenSource{
		AccessToken: token,
	}

	oauthClient := oauth2.NewClient(oauth2.NoContext, tokenSource)
	client := godo.NewClient(oauthClient)

	ctx := context.TODO()

	config, _, err := client.Kubernetes.GetKubeConfig(ctx, cluster.ID)
	if err != nil {
		panic(fmt.Sprintf("could not fetch config file from DO: %v", err))
	}

	kubeConfigFile := string(config.KubeconfigYAML)

	name := fmt.Sprintf("%s-%s", cluster.Name, cluster.ID)

	filePath := path.Join(homeDir, "/.kube/", name)

	err = ioutil.WriteFile(filePath, []byte(kubeConfigFile), 777)
	if err != nil {
		panic(fmt.Sprintf("could not write config filefrom ~/do.conf: %v", err))
	}

	color.Green("+++ Saved kubeconfig for %s to \"%s\"", cluster.Name, filePath)

	return filePath
}

func listClusters(token string) []*godo.KubernetesCluster {

	tokenSource := &TokenSource{
		AccessToken: token,
	}

	oauthClient := oauth2.NewClient(oauth2.NoContext, tokenSource)
	client := godo.NewClient(oauthClient)

	opt := &godo.ListOptions{
		Page:    1,
		PerPage: 200,
	}

	ctx := context.Background()

	clusters, _, err := client.Kubernetes.List(ctx, opt)
	if err != nil {
		panic(fmt.Sprintf("could not list clusters: %v", err))
	}
	return clusters
}

type TokenSource struct {
	AccessToken string
}

func (t *TokenSource) Token() (*oauth2.Token, error) {
	token := &oauth2.Token{
		AccessToken: t.AccessToken,
	}
	return token, nil
}
